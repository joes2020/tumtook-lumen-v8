/**
 * @api {get} /admins/:id Request User information
 * @apiVersion 0.0.1
 * @apiName GetUser
 * @apiGroup Admin
 *
 * @apiParam {Number} id User's unique ID.
 *
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 */

